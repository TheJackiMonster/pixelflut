# Pixelflut: Multiplayer canvas
Self-made pixelflut server and short library for applications with some personal features..

## Idea

This application sets up a server which handles multiple connections from applications using the attached library or another implementation of the concept 'Pixelflut'. The server displays pixels which were drawn by the connected applications directly into the frame-buffer of your tty ( working on linux with '/dev/fb0' ). All applications can send their independent commands and pixels without waiting for others...

## Features

 - GOD-mode to skip every sleep command in the applications thread.
 - BIN-mode to reduce the traffic via binary commands.
 - SIZE-command to receive the resolution of the display.
 - PX-command to draw pixels with RGB or RGBA

## Original concept

More information about pixelflut, its use and origin:
 - https://github.com/defnull/pixelflut
 - https://cccgoe.de/wiki/Pixelflut
