#!/bin/bash

gcc -O3 -L. -lpixelflut test/px_test.c -o px_test

export LD_LIBRARY_PATH=.:$LD_LIBRARY_PATH
./px_test
