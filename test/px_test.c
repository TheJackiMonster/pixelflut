
#include "../src/px_application.h"

int main(int argc, char** argv) {
	px_application* app = px_open_application("127.0.0.1", PX_DEFAULT_PORT); // opens connection

	if (app != 0) {
		px_call_SIZE(app); // Updates: width & height
		
		// Let's fill the screen with RED ( in RGBA: 0xFF0000FF )
		
		for (int y = 0; y < app->height; y++) {
			for (int x = 0; x < app->width; x++) {
				px_call_PX(app, x, y, 0xFF0000FF); // draws one pixel
			}
		}
		
		px_close_application(app); // closes connection
	}
		
	return 0;
}
