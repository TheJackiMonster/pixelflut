/*
 * px_modes.h
 *
 *  Created on: Feb 07, 2017
 *      Author: thejackimonster
 */

#ifndef PX_MODES_H_
#define PX_MODES_H_

#define PX_MODE_NON 0x0
#define PX_MODE_GOD 0x1
#define PX_MODE_BIN 0x2

#endif /* PX_MODES_H_ */
