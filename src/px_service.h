/*
 * px_service.h
 *
 *  Created on: Feb 06, 2017
 *      Author: thejackimonster
 */

#ifndef PX_SERVICE_H_
#define PX_SERVICE_H_

#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "px_connection.h"

typedef struct pixelflut_service {
	char status;
	int sock;
	
	unsigned int total_count;
	unsigned int current_count;
	
	pthread_t tid;
	
	px_connection** connections;
} px_service;

px_service* px_start_service(int port, int backlog, fb_screen* screen);

void px_stop_service(px_service* service);

#endif /* PX_SERVICE_H_ */
