/*
 * px_setup.h
 *
 *  Created on: Feb 08, 2017
 *      Author: thejackimonster
 */

#ifndef PX_SETUP_H_
#define PX_SETUP_H_

#include "px_modes.h"

#define PX_BUFFER_LEN 1024
#define PX_MAX_LINE_LEN 32

#define PX_DEFAULT_PORT 1234
#define PX_DEFAULT_CONNECTION_COUNT 2

#define PX_DEFAULT_WIDTH 640
#define PX_DEFAULT_HEIGHT 480

#endif /* PX_SETUP_H_ */
