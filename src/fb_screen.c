
#include "fb_screen.h"

fb_screen* fb_open_screen(const char* filename) {
	int fd = open(filename, O_RDWR);

	if (!fd) {
		return 0;
	}

	struct fb_fix_screeninfo finfo;
	struct fb_var_screeninfo vinfo;

	if ((ioctl(fd, FBIOGET_FSCREENINFO, &finfo)) || 
	    (ioctl(fd, FBIOGET_VSCREENINFO, &vinfo))) {
		close(fd);
		return 0;
	}

	unsigned short width = (finfo.line_length * 8 / vinfo.bits_per_pixel);
	unsigned short height = vinfo.yres;

	unsigned char element_size = finfo.line_length / width;

	size_t size = width * height * element_size;
	char* buffer = (char*) mmap(0, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

	if ((size_t) buffer == -1) {
		close(fd);
		return 0;
	}

	fb_screen* screen = (fb_screen*) malloc(sizeof(fb_screen));

	screen->fd = fd;

	screen->width = width;
	screen->height = height;

	screen->element_size = element_size;

	screen->buffer_size = size;
	screen->buffer = buffer;

	screen->red.offset = vinfo.red.offset;
	screen->red.mask = ((-1 << vinfo.red.length) ^ -1);

	screen->green.offset = vinfo.green.offset;
	screen->green.mask = ((-1 << vinfo.green.length) ^ -1);

	screen->blue.offset = vinfo.blue.offset;
	screen->blue.mask = ((-1 << vinfo.blue.length) ^ -1);

	screen->transp.offset = vinfo.transp.offset;
	screen->transp.mask = ((-1 << vinfo.transp.length) ^ -1);

	screen->swap_buffer = (char*) malloc(screen->buffer_size);

	memset(screen->swap_buffer, 0, screen->buffer_size);

	return screen;
}

void fb_close_screen(fb_screen* screen) {
	free(screen->swap_buffer);

	munmap(screen->buffer, screen->buffer_size);

	close(screen->fd);

	free(screen);
}

void fb_color(fb_screen* screen, int x, int y, int red, int green, int blue, int transp) {
	if ((x >= 0) && (y >= 0) && (x < screen->width) && (y < screen->height)) {
		int* pixel = (int*) (screen->swap_buffer + ((size_t) x + y * screen->width) * screen->element_size);
			
		*pixel = ((red & screen->red.mask) << screen->red.offset) |
			 ((green & screen->green.mask) << screen->green.offset) |
			 ((blue & screen->blue.mask) << screen->blue.offset) |
			 ((transp & screen->transp.mask) << screen->transp.offset);
	}
}

void fb_clear(fb_screen* screen) {
	memset(screen->buffer, 0, screen->buffer_size);
}

void fb_swap(fb_screen* screen) {
	memcpy(screen->buffer, screen->swap_buffer, screen->buffer_size);
}

void fb_sync(fb_screen* screen) {
	fb_swap(screen);
}

