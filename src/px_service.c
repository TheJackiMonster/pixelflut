
#include "px_service.h"

void* px_run_service(void* argv);

px_service* px_start_service(int port, int backlog, fb_screen* screen) {
	int sock = socket(AF_INET, SOCK_STREAM, 0);

	if (sock < 0) {
		return 0;
	}

	int optval = 1;

	setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (const void*) &optval, sizeof(int));

	struct sockaddr_in addr;

	bzero((char*) &addr, sizeof(addr));
	
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = htonl(INADDR_ANY);

	if (port < 0) {
		addr.sin_port = htons((unsigned short) PX_DEFAULT_PORT);
	} else {
		addr.sin_port = htons((unsigned short) port);
	}

	if (bind(sock, (struct sockaddr*) &addr, sizeof(addr)) < 0) {
		close(sock);
		return 0;
	}

	if (listen(sock, backlog) < 0) {
		close(sock);
		return 0;
	}

	px_service* service = (px_service*) malloc(sizeof(px_service));
	
	service->status = 0;
	service->sock = sock;

	service->total_count = PX_DEFAULT_CONNECTION_COUNT;
	service->current_count = 0;

	if (backlog > service->total_count) {
		service->total_count = backlog;
	}
	
	service->connections = (px_connection**) malloc(sizeof(void*) * service->total_count);
	
	for (int i = 0; i < service->total_count; i++) {
		service->connections[i] = 0;
	}
	
	void* argv [2];
	
	argv[0] = (void*) service;
	argv[1] = (void*) screen;
	
	if (pthread_create(&(service->tid), 0, px_run_service, argv) != 0) {
		free(service->connections);

		close(service->sock);

		free(service);
		return 0;
	} else {
		fb_sync(screen);
	}
	
	return service;
}

void px_stop_service(px_service* service) {
	service->status |= 1;
	
	// pthread_join(service->tid, 0);
	
	while (service->current_count-- > 0) {
		if (service->connections[ service->current_count ] != 0) {
			px_close_connection((px_connection*) service->connections[ service->current_count ]);
		}
	}

	free(service->connections);

	close(service->sock);

	free(service);
}

void* px_run_service(void* argv) {
	px_service* service = (px_service*) ((void**) argv)[0];
	fb_screen* screen = ((fb_screen*) ((void**) argv)[1]);

	int sock;

	fb_sync(screen);

	while (service->status == 0) {
		struct sockaddr_in c_addr;
		int c_addr_len = sizeof(c_addr);

		sock = accept(service->sock, (struct sockaddr*) &c_addr, &c_addr_len);

		if (sock < 0) {
			continue;
		}

		int i = 0;

		while (i < service->current_count) {
			px_connection* connection = (px_connection*) service->connections[i];

			if (connection->status == 2) {
				px_close_connection(connection);

				if (i + 1 < service->current_count) {
					service->connections[i] = service->connections[i + 1];
				} else {
					service->connections[i] = 0;
				}

				service->current_count--;
			} else {
				i++;
			}
		}

		if (service->current_count == service->total_count) {
			void* new_ptr = realloc(service->connections, sizeof(void*) * service->total_count * 2);

			if (new_ptr != 0) {
				service->connections = (px_connection**) new_ptr;
				service->total_count *= 2;

				for (i = service->current_count + 1; i < service->total_count; i++) {
					service->connections[i] = 0;
				}
			} else {
				continue;
			}
		}

		px_connection* connection = px_open_connection(sock);
		
		void* c_argv [2];
		
		c_argv[0] = (void*) connection;
		c_argv[1] = (void*) screen;
		
		if (pthread_create(&(connection->tid), 0, px_run_connection, c_argv) != 0) {
			px_close_connection(connection);
			continue;
		} else {
			fb_sync(screen);
		}

		service->connections[ service->current_count++ ] = connection;
	}

	service->status |= 2;
	
	fb_sync(screen);

	return 0;
}

