/*
 * fb_screen.h
 *
 *  Created on: Feb 06, 2017
 *      Author: thejackimonster
 */

#ifndef FB_SCREEN_H_
#define FB_SCREEN_H_

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <linux/fb.h>

typedef struct framebuffer_colorset {
	int offset;
	int mask;
} fb_colorset;

typedef struct framebuffer_screen {
	int fd;
	
	unsigned short width;
	unsigned short height;
	
	unsigned char element_size;
	
	size_t buffer_size;
	char* buffer;
	
	fb_colorset red;
	fb_colorset green;
	fb_colorset blue;
	fb_colorset transp;
	
	char* swap_buffer;
} fb_screen;

fb_screen* fb_open_screen(const char* filename);

void fb_close_screen(fb_screen* screen);

void fb_clear(fb_screen* screen);

void fb_color(fb_screen* screen, int x, int y, int red, int green, int blue, int transp);

void fb_swap(fb_screen* screen);

void fb_sync(fb_screen* screen);

#endif /* FB_SCREEN_H_ */
