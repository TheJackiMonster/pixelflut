
#include <stdlib.h>
#include <signal.h>

#include "px_service.h"

char interrupted;

void interrupt_handler(int signal) {
	if (signal == SIGINT) {
		interrupted = 1;
	}
}

int main(int argc, char** argv) {
	int port;
	
	if (argc > 1) {
		port = atoi(argv[1]);
	} else {
		port =  PX_DEFAULT_PORT;
	}
	
	interrupted = 0;
	
	signal(SIGINT, interrupt_handler);
	
	fb_screen* screen = fb_open_screen("/dev/fb0");

	if (screen != 0) {
		system("stty -echo");
		system("setterm -cursor off");
		
		fb_clear(screen);

		px_service* service = px_start_service(port, 2, screen);
		
		if (service != 0) {
			do {
				fb_swap(screen);
			} while ((!interrupted) && (service->status == 0));
			
			px_stop_service(service);
		}
		
		fb_clear(screen);
		
		system("setterm -cursor on");
		system("stty echo");
	}
	
	return 0;
}

