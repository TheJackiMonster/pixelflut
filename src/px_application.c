
#include "px_application.h"

px_application* px_open_application(const char* hostname, int port) {
	int sock = socket(AF_INET, SOCK_STREAM, 0);

	if (sock < 0) {
		return 0;
	}
	
	struct hostent* server;
	
	server = gethostbyname(hostname);
	
	if (server == 0) {
		return 0;
	}
	
	struct sockaddr_in addr;
	
	bzero((char*) &addr, sizeof(addr));
	
	addr.sin_family = AF_INET;
	
	bcopy((char*) server->h_addr, (char*) &(addr.sin_addr.s_addr), server->h_length);
	
	if (port < 0) {
		addr.sin_port = htons((unsigned short) PX_DEFAULT_PORT);
	} else {
		addr.sin_port = htons((unsigned short) port);
	}
	
	if (connect(sock, (struct sockaddr*) &addr, sizeof(addr)) < 0) {
		close(sock);
		return 0;
	}
	
	px_application* application = (px_application*) malloc(sizeof(px_application));
	
	application->status = 0;
	application->sock = sock;
	
	application->width = PX_DEFAULT_WIDTH;
	application->height = PX_DEFAULT_HEIGHT;
	
	application->mode = PX_MODE_NON;

	application->data_offset = 0;
	
	return application;
}

void px_close_application(px_application* application) {
	application->status |= 1;
	
	px_call_QUIT(application);
	
	close(application->sock);
	
	free(application);
}

void px_write_data(px_application* application, char* data, unsigned int len, char force) {
	const unsigned int max_len = PX_BUFFER_LEN - application->data_offset;

	unsigned int data_len;

	if (len > max_len) {
		data_len = max_len;
	} else {
		data_len = len;
	}

	memcpy(application->buffer + application->data_offset, data, data_len);
	application->data_offset += data_len;

	if ((force) || (application->data_offset >= PX_BUFFER_LEN)) {
		write(application->sock, application->buffer, application->data_offset);
		application->data_offset = 0;
		
		if (len > max_len) {
			px_write_data(application, data + data_len, len - data_len, force);
		}
	}
}

void px_call_GODMODE(px_application* application, char flag) {
	if ((application->mode & PX_MODE_BIN) == 0) {
		px_write_data(application, "GODMODE ", 8, 0);
		
		if (flag) {
			px_write_data(application, "on\n", 3, 0);
		} else {
			px_write_data(application, "off\n", 4, 0);
		}
	} else {
		px_write_data(application, "G", 1, 0);
		px_write_data(application, &flag, sizeof(flag), 0);
	}

	if (flag) {
		application->mode |= PX_MODE_GOD;
	} else {
		application->mode &= (-1 ^ PX_MODE_GOD);
	}
}

void px_call_BINMODE(px_application* application, char flag) {
	if ((application->mode & PX_MODE_BIN) == 0) {
		px_write_data(application, "BINMODE ", 8, 0);
		
		if (flag) {
			px_write_data(application, "on\n", 3, 0);
		} else {
			px_write_data(application, "off\n", 4, 0);
		}
	} else {
		px_write_data(application, "B", 1, 0);
		px_write_data(application, &flag, sizeof(flag), 0);
	}

	if (flag) {
		application->mode |= PX_MODE_BIN;
	} else {
		application->mode &= (-1 ^ PX_MODE_BIN);
	}
}

void px_call_SIZE(px_application* application) {
	if ((application->mode & PX_MODE_BIN) == 0) {
		px_write_data(application, "SIZE\n", 5, 1);
		
		char buffer [ PX_MAX_LINE_LEN ];
		
		size_t data_length = read(application->sock, buffer, PX_MAX_LINE_LEN);

		if ((data_length >= 8) && (memcmp(buffer, "SIZE ", 5) == 0) && (buffer[--data_length] == '\n')) {
			int wo = 5;
			int ho = wo;
			int eoh;
			
			while ((ho < data_length) && (isdigit(buffer[ho]))) {
				ho++;
			}

			if ((ho < data_length) && (buffer[ho] == ' ')) {
				buffer[ho++] = 0;
				eoh = ho;

				while ((eoh < data_length) && (isdigit(buffer[eoh]))) {
					eoh++;
				}

				if (eoh == data_length) {
					buffer[data_length] = 0;

					application->width = (unsigned short) (strtol(buffer + wo, 0, 10) & 0xFFFF);
					application->height = (unsigned short) (strtol(buffer + ho, 0, 10) & 0xFFFF);
				}
			}
		}
	} else {
		px_write_data(application, "S", 1, 1);
		
		read(application->sock, &(application->width), sizeof(application->width));
		read(application->sock, &(application->height), sizeof(application->height));
	}
}

void px_call_PX(px_application* application, int x, int y, int rgba) {
	if ((application->mode & PX_MODE_BIN) == 0) {
		char buffer [ PX_MAX_LINE_LEN ];
		int data_len = 0;
		
		if ((rgba & 0xFF) == 0xFF) {
			data_len = snprintf(buffer, PX_MAX_LINE_LEN, "PX %d %d %06X\n", x, y, (rgba >> 8) & 0xFFFFFF);
		} else {
			data_len = snprintf(buffer, PX_MAX_LINE_LEN, "PX %d %d %08X\n", x, y, rgba);
		}

		px_write_data(application, buffer, data_len, 0);
	} else {
		char data [9];

		if ((rgba & 0xFF) == 0xFF) {
			*((char*) (data + 0)) = 'p';
		} else {
			*((char*) (data + 0)) = 'P';
		}

		*((short*) (data + 1)) = (short) (x & 0xFFFF);
		*((short*) (data + 3)) = (short) (y & 0xFFFF);
		
		*((char*) (data + 5)) = (char) ((rgba >> 24) & 0xFF);
		*((char*) (data + 6)) = (char) ((rgba >> 16) & 0xFF);
		*((char*) (data + 7)) = (char) ((rgba >>  8) & 0xFF);
		
		if ((rgba & 0xFF) != 0xFF) {
			*((char*) (data + 8)) = (char) (rgba & 0xFF);

			px_write_data(application, data, 9, 0);
		} else {
			px_write_data(application, data, 8, 0);
		}
	}
}

void px_call_QUIT(px_application* application) {
	if ((application->mode & PX_MODE_BIN) == 0) {
		px_write_data(application, "QUIT\n", 5, 1);
	} else {
		px_write_data(application, "Q", 1, 1);
	}
	
	application->status |= 1;
}

