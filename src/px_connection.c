
#include "px_connection.h"

px_connection* px_open_connection(int sock) {
	px_connection* connection = (px_connection*) malloc(sizeof(px_connection));

	connection->status = 0;
	connection->sock = sock;

	connection->data_offset = 0;
	connection->data_length = 0;

	connection->line_length = 0;

	connection->mode = PX_MODE_NON;
	
	return connection;
}

void px_close_connection(px_connection* connection) {
	connection->status |= 1;
	
	pthread_join(connection->tid, 0);

	close(connection->sock);

	free(connection);
}

char px_get_next(px_connection* connection) {
	if (connection->data_length <= 0) {
		int r = read(connection->sock, connection->buffer + connection->data_offset, PX_BUFFER_LEN - connection->data_offset);

		if (r < 0) {
			connection->status |= (r < 0);
			return '\n';
		} else {
			connection->data_length = r;
		}
	}

	char ch = *(connection->buffer + connection->data_offset);

	connection->data_offset++;
	connection->data_length--;

	if (connection->data_offset >= PX_BUFFER_LEN) {
		connection->data_offset = 0;
	}

	return ch;
}

void px_update_line(px_connection* connection) {
	int i = 0;
	char ch;

	do {
		ch = px_get_next(connection);

		if (ch == '\n') {
			break;
		} else {
			connection->line[i++] = ch;
		}
	} while (i < PX_MAX_LINE_LEN - 1);

	connection->line_length = i;
	connection->line[i] = 0;
}

void px_command_line(fb_screen* screen, px_connection* connection) {
	px_update_line(connection);

	if ((connection->line_length >= 10) && (memcmp(connection->line, "GODMODE o", 9) == 0)) {
		if ((connection->line_length >= 11) && (memcmp(connection->line + 9, "ff", 2) == 0)) {
			connection->mode &= (-1 ^ PX_MODE_GOD);
		} else
		if (memcmp(connection->line + 9, "n", 1) == 0) {
			connection->mode |= PX_MODE_GOD;
		}
	} else
	if ((connection->line_length >= 10) && (memcmp(connection->line, "BINMODE o", 9) == 0)) {
		if ((connection->line_length >= 11) && (memcmp(connection->line + 9, "ff", 2) == 0)) {
			connection->mode &= (-1 ^ PX_MODE_BIN);
		} else
		if (memcmp(connection->line + 9, "n", 1) == 0) {
			connection->mode |= PX_MODE_BIN;
		}
	} else
	if ((connection->line_length >= 4) && (memcmp(connection->line, "SIZE", 4) == 0)) {
		dprintf(connection->sock, "SIZE %u %u\n", screen->width, screen->height);
	} else
	if ((connection->line_length >= 13) && (memcmp(connection->line, "PX ", 3) == 0)) {
		int xo = 3;
		int yo = xo;
		int co;
		int eoc;

		while ((yo < connection->line_length) && (isdigit(connection->line[yo]))) {
			yo++;
		}

		if ((yo < connection->line_length) && (connection->line[yo] == ' ')) {
			connection->line[yo++] = 0;
			co = yo;

			while ((co < connection->line_length) && (isdigit(connection->line[co]))) {
				co++;
			}

			if ((co < connection->line_length) && (connection->line[co] == ' ')) {
				connection->line[co++] = 0;
				eoc = co;

				while ((eoc < connection->line_length) && (isxdigit(connection->line[eoc]))) {
					eoc++;
				}

				if (eoc == connection->line_length) {
					int x = (int) strtol(connection->line + xo, 0, 10);
					int y = (int) strtol(connection->line + yo, 0, 10);

					long c = (long) strtol(connection->line + co, 0, 16);

					int red, green, blue, alpha;

					switch (eoc - co) {
					case 6:
						red   = (int) (0xFF & (c >> 16));
						green = (int) (0xFF & (c >> 8));
						blue  = (int) (0xFF & (c));

						fb_color(screen, x, y, red, green, blue, 0x00);

						break;
					case 8:
						red   = (int) (0xFF & (c >> 24));
						green = (int) (0xFF & (c >> 16));
						blue  = (int) (0xFF & (c >> 8));
						alpha = (int) (0xFF & (c));

						fb_color(screen, x, y, red, green, blue, 0xFF - alpha);

						break;
					default:
						break;
					}
				}
			}
		}
	} else
	if ((connection->line_length >= 4) && (memcmp(connection->line, "QUIT", 4) == 0)) {
		connection->status |= 1;
	}
}

void px_command_bin(fb_screen* screen, px_connection* connection) {
	char code = px_get_next(connection);

	if (code == 'G') {
		if (px_get_next(connection)) {
			connection->mode |= PX_MODE_GOD;
		} else {
			connection->mode &= (-1 ^ PX_MODE_GOD);
		}
	} else
	if (code == 'B') {
		if (px_get_next(connection)) {
			connection->mode |= PX_MODE_BIN;
		} else {
			connection->mode &= (-1 ^ PX_MODE_BIN);
		}
	} else
	if (code == 'S') {
		write(connection->sock, &(screen->width), sizeof(screen->width));
		write(connection->sock, &(screen->height), sizeof(screen->height));
	} else
	if ((code == 'p') || (code == 'P')) {
		int x = (0xFF & px_get_next(connection)) | ((0xFF & px_get_next(connection)) << 8);
		int y = (0xFF & px_get_next(connection)) | ((0xFF & px_get_next(connection)) << 8);
		
		int red   = 0xFF & px_get_next(connection);
		int green = 0xFF & px_get_next(connection);
		int blue  = 0xFF & px_get_next(connection);

		switch (code) {
		case 'p':
			fb_color(screen, x, y, red, green, blue, 0x00);
			break;
		case 'P':
			fb_color(screen, x, y, red, green, blue, 0xFF - (0xFF & px_get_next(connection)));
			break;
		default:
			break;
		}
	} else
	if (code == 'Q') {
		connection->status |= 1;
	}
}

void* px_run_connection(void* argv) {
	px_connection* connection = (px_connection*) ((void**) argv)[0];
	fb_screen* screen = ((fb_screen*) ((void**) argv)[1]);

	fb_sync(screen);

	while (connection->status == 0) {
		if ((connection->mode & PX_MODE_BIN) == 0) {
			px_command_line(screen, connection);
		} else {
			px_command_bin(screen, connection);
		}

		if ((connection->mode & PX_MODE_GOD) == 0) {
			usleep(10);
		}
	}

	connection->status |= 2;

	fb_sync(screen);

	return 0;
}

