/*
 * px_connection.h
 *
 *  Created on: Feb 07, 2017
 *      Author: thejackimonster
 */

#ifndef PX_CONNECTION_H_
#define PX_CONNECTION_H_

#include <ctype.h>
#include <pthread.h>

#include "fb_screen.h"

#include "px_setup.h"

typedef struct pixelflut_connection {
	char status;
	int sock;
	
	char buffer [ PX_BUFFER_LEN ];
	unsigned int data_offset;
	unsigned int data_length;
	
	char line [ PX_MAX_LINE_LEN ];
	unsigned int line_length;
	
	pthread_t tid;
	
	char mode;
} px_connection;

px_connection* px_open_connection(int sock);

void px_close_connection(px_connection* connection);

void* px_run_connection(void* arg);

#endif /* PX_CONNECTION_H_ */
