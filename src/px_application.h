/*
 * px_connection.h
 *
 *  Created on: Feb 07, 2017
 *      Author: thejackimonster
 */

#ifndef PX_APPLICATION_H_
#define PX_APPLICATION_H_

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "px_setup.h"

typedef struct pixelflut_application {
	char status;
	int sock;
	
	unsigned short width;
	unsigned short height;
	
	char mode;

	char buffer [ PX_BUFFER_LEN ];
	unsigned int data_offset;
} px_application;

px_application* px_open_application(const char* hostname, int port);

void px_close_application(px_application* application);

void px_call_GODMODE(px_application* application, char flag);

void px_call_BINMODE(px_application* application, char flag);

void px_call_SIZE(px_application* application);

void px_call_PX(px_application* application, int x, int y, int argb);

void px_call_QUIT(px_application* application);

#endif /* PX_APPLICATION_H_ */
