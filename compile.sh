#!/bin/bash
gcc -O3 -Lsrc/ -lpthread src/fb_screen.c src/px_connection.c src/px_service.c src/px_main.c -o pixelflut
gcc -O3 -Lsrc/ -fpic -shared src/px_application.c -o libpixelflut.so
